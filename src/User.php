<?php

namespace Ci;

class User
{
    /**
     * User height
     *
     * @var float
     */
    private $height;

    /**
     * User name
     *
     * @var string
     */
    private $name;

    /**
     * User weight
     *
     * @var float
     */
    private $weight;

    /**
     * Set name for this user object
     *
     * @param name string
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * Set user height
     *
     * @param height float
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * Set user weight
     *
     * @param weight float
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * Get user BMI
     *
     * @return float
     */
    public function getBmi()
    {
        $bmi = ($this->weight / ($this->height * $this->height)) * 10000;

        return $bmi;
    }
}
