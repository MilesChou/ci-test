<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';

$config = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];

$app = new \Slim\App($config);

$app->get('/{name}/{height}/{weight}', function (Request $request, Response $response) {
    $name = $request->getAttribute('name');
    $height = $request->getAttribute('height');
    $weight = $request->getAttribute('weight');

    $user = new Ci\User($name);
    $user->setHeight($height);
    $user->setWeight($weight);

    $data = [
        'name' => $name,
        'height' => $height,
        'weight' => $weight,
        'bmi' => $user->getBmi(),
    ];

    return $response->withJson($data, 200);
});

$app->get('/hello/{name}', function (Request $request, Response $response) {
    $name = $request->getAttribute('name');
    $response->getBody()->write("Hello, $name");

    return $response;
});

$app->run();
