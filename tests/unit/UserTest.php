<?php
namespace Ci;

class UserTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /**
     * Test BMI function
     */
    public function testBmiWhenHeight100AndWeight50WillReturn50()
    {
        // Arrange
        $excepted = 50;
        $height = 100;
        $weight = 50;
        $name = "somebody";

        $target = new User($name);
        $target->setHeight($height);
        $target->setWeight($weight);

        // Act
        $actual = $target->getBmi();

        // Assert
        $this->assertEquals($excepted, $actual);
    }
}
