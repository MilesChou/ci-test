<?php
$I = new ApiTester($scenario);
$I->wantTo('perform BMI cals and see result');
$I->sendGet('somebody/100/50');
$I->seeResponseCodeIs(200);
$I->seeResponseContainsJson(['bmi' => 50]);
